<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
    protected $table = 'countries';
    public $timestamps = false;

    public static function add_country($long_name, $short_name, $id_place) {
        $country = new Country();
        $country->long_name = $long_name;
        $country->short_name = $short_name;
        $country->id_place = $id_place;
        $country->save();

        return $country->id;
    }

    public static function get_country($long_name) {
        return \DB::table('countries')
            ->select('countries.*')
            ->where('long_name', '=', $long_name)
            ->first();
    }

    public static function update_place_id($id, $id_place) {
        \DB::table('countries')
            ->where('id', $id)
            ->update(['id_place' => $id_place]);
    }
}