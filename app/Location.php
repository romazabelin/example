<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';

    public static function get_location($city) {
        if ($city) {
            $result = \DB::table('location')
                ->select(
                    'location.name as location_name',
                    'location.id as location_id'
                )
                ->where('name', 'like', '%' . $city . '%')
                ->first();
        } else {
            $result = false;
        }

        return $result;
    }

    public static function update_place_id($id_location, $id_place) {
        \DB::table('location')
            ->where('id', $id_location)
            ->update(['id_place' => $id_place]);
    }

    public static function update_country_id($id_location, $id_country) {
        \DB::table('location')
            ->where('id', $id_location)
            ->update(['id_country' => $id_country]);
    }
}
