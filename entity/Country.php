<?php

namespace EuroCommerce\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Country
 *
 * @ORM\Entity(repositoryClass="EuroCommerce\AppBundle\Repository\CountryRepo")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="country")
 */
class Country
{

    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=2, nullable=true, unique=true)
     */
    private $code;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $is_active = true;

    /**
     * @ORM\ManyToMany(targetEntity="EuroCommerce\AppBundle\Entity\Offer", mappedBy="countries")
     */
    private $offers;

    /**
     *
     * @ORM\OneToMany(targetEntity="EuroCommerce\AppBundle\Entity\Offer", mappedBy="producingCountry", cascade={ "persist", "remove"}, orphanRemoval=true)
     */
    private $offersProducing;

    /**
     * @var integer $continent
     *
     * @ORM\ManyToOne(targetEntity="EuroCommerce\AppBundle\Entity\Continent", inversedBy="countries")
     */
    private $continent;

    /**
     * @ORM\OneToMany(targetEntity="EuroCommerce\AppBundle\Entity\WaterPort", mappedBy="country", cascade={ "persist", "remove"}, orphanRemoval=true)
     */
    private $ports;


    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Blog
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getName() {
        return $this->translate()->getName();
    }

    public function setName($name)
    {
        $this->translate()->setName($name);

        return $this;
    }

    /**
     * Add offer
     *
     * @param \EuroCommerce\AppBundle\Entity\Offer $offer
     *
     * @return Country
     */
    public function addOffer(\EuroCommerce\AppBundle\Entity\Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \EuroCommerce\AppBundle\Entity\Offer $offer
     */
    public function removeOffer(\EuroCommerce\AppBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set continent
     *
     * @param \EuroCommerce\AppBundle\Entity\Continent $continent
     *
     * @return Country
     */
    public function setContinent(\EuroCommerce\AppBundle\Entity\Continent $continent = null)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return \EuroCommerce\AppBundle\Entity\Continent
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Add offersProducing
     *
     * @param \EuroCommerce\AppBundle\Entity\Offer $offersProducing
     *
     * @return Country
     */
    public function addOffersProducing(\EuroCommerce\AppBundle\Entity\Offer $offersProducing)
    {
        $this->offersProducing[] = $offersProducing;

        return $this;
    }

    /**
     * Remove offersProducing
     *
     * @param \EuroCommerce\AppBundle\Entity\Offer $offersProducing
     */
    public function removeOffersProducing(\EuroCommerce\AppBundle\Entity\Offer $offersProducing)
    {
        $this->offersProducing->removeElement($offersProducing);
    }

    /**
     * Get offersProducing
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffersProducing()
    {
        return $this->offersProducing;
    }

    public function getPorts()
    {
        return $this->ports;
    }

    /**
     * Add port
     *
     * @param \EuroCommerce\AppBundle\Entity\WaterPort $port
     *
     * @return Country
     */
    public function addPort(\EuroCommerce\AppBundle\Entity\WaterPort $port)
    {
        $this->ports[] = $port;

        return $this;
    }

    /**
     * Remove port
     *
     * @param \EuroCommerce\AppBundle\Entity\WaterPort $port
     */
    public function removePort(\EuroCommerce\AppBundle\Entity\WaterPort $port)
    {
        $this->ports->removeElement($port);
    }
}
