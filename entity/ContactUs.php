<?php

namespace EuroCommerce\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact Us
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="contact_us")
 */
class ContactUs
{

    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=true)
     * @Assert\NotBlank(message="Please enter product name.")
     */
    private $name;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=30, nullable=true)
     * @Assert\NotBlank(message="Please enter email.")
     */
    private $email;

    /**
     * @var string $message
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     * @Assert\NotBlank(message="Please enter message.")
     */
    private $message;

    /**
     * @ORM\Column(name="sync_with_bitrix", type="boolean", nullable=true)
     */
    private $sync_with_bitrix;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactUs
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ContactUs
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ContactUs
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set syncWithBitrix
     *
     * @param boolean $syncWithBitrix
     *
     * @return ContactUs
     */
    public function setSyncWithBitrix($syncWithBitrix)
    {
        $this->sync_with_bitrix = $syncWithBitrix;

        return $this;
    }

    /**
     * Get syncWithBitrix
     *
     * @return boolean
     */
    public function getSyncWithBitrix()
    {
        return $this->sync_with_bitrix;
    }
}
