<?php

namespace EuroCommerce\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Continent
 *
 * @ORM\Entity(repositoryClass="EuroCommerce\AppBundle\Repository\ContinentRepo")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="continent")
 */
class Continent
{

    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=2, nullable=true, unique=true)
     */
    private $code;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $is_active = true;

    /**
     * @ORM\OneToMany(targetEntity="EuroCommerce\AppBundle\Entity\Country", mappedBy="continent", cascade={ "persist", "remove"}, orphanRemoval=true)
     */
    private $countries;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Continent
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Continent
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    public function getName() {
        return $this->translate()->getName();
    }

    public function setName($name)
    {
        $this->translate()->setName($name);

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add country
     *
     * @param \EuroCommerce\AppBundle\Entity\Country $country
     *
     * @return Continent
     */
    public function addCountry(\EuroCommerce\AppBundle\Entity\Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \EuroCommerce\AppBundle\Entity\Country $country
     */
    public function removeCountry(\EuroCommerce\AppBundle\Entity\Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
