<?php

namespace EuroCommerce\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Entity(repositoryClass="EuroCommerce\AppBundle\Repository\CategoryRepo")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="category")
 */
class Category
{

    const CODE_BEEF = 'beef';
    const CODE_PORK = 'pork';
    const CODE_POULTRY = 'poultry';

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable
        ;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
    * One Category has Many Categories.
    * @ORM\OneToMany(targetEntity="EuroCommerce\AppBundle\Entity\Category", mappedBy="parent")
    */
    private $children;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="EuroCommerce\AppBundle\Entity\Category", inversedBy="children")
     */
    private $parent;

    /**
     * @var integer $parentId
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
    * @var string $svg_id
    *
    * @ORM\Column(name="svg_id", type="string", length=50, nullable=true, unique=true)
    */
    private $svgId;

    /**
     * @ORM\Column(
     *     type="string", length=10, nullable=true, unique=true,
     *     options={"comment":"Required code for main categories. For beef: beef, pork: pork, poultry: poultry etc."}
     * )
     */
    private $code;

    /**
     *
     * @ORM\OneToMany(targetEntity="EuroCommerce\AppBundle\Entity\Offer", mappedBy="category", cascade={ "persist", "remove"}, orphanRemoval=true)
     */
    private $offers;

    static public $aCodes = [
        self::CODE_BEEF => self::CODE_BEEF,
        self::CODE_PORK => self::CODE_PORK,
        self::CODE_POULTRY => self::CODE_POULTRY,
    ];


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Category
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return Category
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set svgId
     *
     * @param string $svgId
     *
     * @return Category
     */
    public function setSvgId($svgId)
    {
        $this->svgId = $svgId;

        return $this;
    }

    /**
     * Get svgId
     *
     * @return string
     */
    public function getSvgId()
    {
        return $this->svgId;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Category
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add child
     *
     * @param \EuroCommerce\AppBundle\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(\EuroCommerce\AppBundle\Entity\Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \EuroCommerce\AppBundle\Entity\Category $child
     */
    public function removeChild(\EuroCommerce\AppBundle\Entity\Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \EuroCommerce\AppBundle\Entity\Category $parent
     *
     * @return Category
     */
    public function setParent(\EuroCommerce\AppBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \EuroCommerce\AppBundle\Entity\Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add offer
     *
     * @param \EuroCommerce\AppBundle\Entity\Offer $offer
     *
     * @return Category
     */
    public function addOffer(\EuroCommerce\AppBundle\Entity\Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \EuroCommerce\AppBundle\Entity\Offer $offer
     */
    public function removeOffer(\EuroCommerce\AppBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    public function __toString()
    {
        return (string) $this->translate($this->getCurrentLocale())->getName();
    }

    public function setName($name)
    {
        $this->translate()->setName($name);

        return $this;
    }

    public function getName() {
        return $this->translate()->getName();
    }

    public function setDescription($description)
    {
        $this->translate()->setDescription($description);

        return $this;
    }

    public function getDescription()
    {
        return $this->translate()->getDescription();
    }

}
