<?php
namespace EuroCommerce\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EuroCommerce\AppBundle\Entity\Notice;
use EuroCommerce\AppBundle\Entity\NoticeTranslation;

class NoticeRepo extends EntityRepository
{
    /**
     * Get count of unread notices
     *
     * @param int $user User id
     * @return int
     */
    public function getCountUnread($user)
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select('count(n.id)')
            ->from(Notice::class, 'n')
            ->where('n.user = :user')
            ->andWhere('n.is_read = false')
            ->setParameter('user', $user);
        return $builder->getQuery()->getSingleScalarResult();
    }

    /**
     * Get all notices by user
     *
     * @param int $user User id
     * @param string $locale
     *
     * @return array
     */
    public function getNoticesByUser($user, $locale)
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select(
                [
                    'n.id', 'n.source', 'n.path', 'n.createdAt',
                    'nt.title AS title', 'nt.description AS description'
                ]
            )
            ->from(Notice::class, 'n')

            ->leftJoin(
                NoticeTranslation::class,
                'nt',
                'WITH',
                'n.id = nt.translatable'
            )

            ->where('n.user = :user')
            ->setParameter('user', $user)

            ->andWhere('nt.locale = :locale')
            ->setParameter('locale', $locale)

            ->orderBy('n.createdAt', 'DESC');

        return $builder->getQuery()->getResult();
    }
}