<?php
namespace EuroCommerce\AppBundle\Repository;

use EuroCommerce\AppBundle\Entity\PackagingType;

class PackagingTypeRepo extends BaseRepo {
    public function getPackagingTypesForOffer($id_offer) {
        return $this->getEntityManager()->createQueryBuilder()
            ->select("pt")
            ->from(PackagingType::class, 'pt')
            ->where("pt.isPublish = :is_publish")
            ->orWhere("pt.id_for_offer = :id_for_offer")
            ->setParameter('is_publish', 1)
            ->setParameter('id_for_offer', $id_offer)
            ->getQuery()
            ->getResult();
    }
}