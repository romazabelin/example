<?php

namespace EuroCommerce\AppBundle\Repository;

use EuroCommerce\AppBundle\Entity\Category;
use EuroCommerce\AppBundle\Entity\CategoryTranslation;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;

class CategoryRepo extends BaseRepo
{

    use ORMBehaviors\Tree\Tree;

    /**
     * @param array $params
     * @param array $fieldsA
     * @return array
     */
    public function getCategories($params = [], $fieldsA = [])
    {
        $fields = $fieldsA ? implode(',', $fieldsA) : 'c';

        $locale = $this->getParam($params, 'locale', 'en');
        $returnArray = $this->getParam($params, 'returnArray', false);
        $byParentId = $this->getParam($params, 'byParentId');
        $parentId = $this->getParam($params, 'parentId', null);

        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select($fields)
            ->from(Category::class, 'c');

        $builder->innerJoin(
            CategoryTranslation::class,
            'c_tr',
            'WITH',
            'c.id = c_tr.translatable'
        );

        if ($locale) {
            $builder->andWhere('c_tr.locale = :locale')
                ->setParameter('locale', $locale);
        }

        if ($byParentId) {
            if ($parentId) {
                $builder->andWhere('c.parentId = :parentId')
                    ->setParameter('parentId', $parentId);
            } else {
                $builder->andWhere('c.parentId IS NULL');
            }
        }

        if ($returnArray) {
            $result = $builder->getQuery()->getArrayResult();
        } else {
            $result = $builder->getQuery()->getResult();
        }

        return $result;
    }

    /**
     * Get all of children categories
     *
     * @return array
     */
    public function getChildren()
    {
        $params['returnArray'] = true;
        $fields = ['c.id', 'c.parentId AS parent_id'];
        $categories = $this->getCategories($params, $fields);

        $parentsIds = [];
        foreach ($categories as $category) {
            $parentsIds[$category['parent_id']] = $category['parent_id'];
        }

        $children = [];
        foreach ($categories as $category) {
            if ( !isset($parentsIds[$category['id']]) ) {
                $children[$category['id']] = $category['id'];
            }
        }

        return $children;
    }
}